package com.twuc.webApp.yourTurn;

import com.twuc.webApp.simplest.*;
import com.twuc.webApp.simplest.scanning.Logger;
import com.twuc.webApp.simplest.scanning.Logger2;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class SimpleTest {

    @Test
    public void interfaceTest(){
        AnnotationConfigApplicationContext ctx =  new AnnotationConfigApplicationContext("com.twuc.webApp.simplest");
        InterfaceOne imp1 = ctx.getBean(InterfaceOne.class);
        InterfaceOneImpl imp2 = ctx.getBean(InterfaceOneImpl.class);
        assertSame(imp1,imp2);
    }

    @Test
    public void extendTest(){
        AnnotationConfigApplicationContext ctx =  new AnnotationConfigApplicationContext("com.twuc.webApp.simplest");
        ExtendOne ext1 = ctx.getBean(ExtendOne.class);
        Extend ext2 = ctx.getBean(Extend.class);
        assertSame(ext1,ext2);
    }

    @Test
    public void absExtendTest(){
        AnnotationConfigApplicationContext ctx =  new AnnotationConfigApplicationContext("com.twuc.webApp.simplest");
        AbstractBaseClass ext1 = ctx.getBean(AbstractBaseClass.class);
        DerivedClass ext2 = ctx.getBean(DerivedClass.class);
        assertSame(ext1,ext2);
    }

    @Test
    public void prototypeTest(){
        AnnotationConfigApplicationContext ctx =  new AnnotationConfigApplicationContext("com.twuc.webApp.simplest");
        SimplePrototypeScopeClass pc1 = ctx.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass pc2 = ctx.getBean(SimplePrototypeScopeClass.class);
        assertNotEquals(pc1,pc2);
    }

    @Test
    public void LifeCycleTest(){
        AnnotationConfigApplicationContext ctx =  new AnnotationConfigApplicationContext("com.twuc.webApp.simplest");
        Logger logger = ctx.getBean(Logger.class);
        logger.log("start");
        InterfaceOne class1 = ctx.getBean(InterfaceOne.class);
        SimplePrototypeScopeClass class2 = ctx.getBean(SimplePrototypeScopeClass.class);
        logger.log("end");
        String s = logger.getLog();
        assertEquals("created normal class\n" +
                "start\n" +
                "created prototype class\n" +
                "end\n",s);

    }

    @Test
    public void TimesTest(){
        AnnotationConfigApplicationContext ctx =  new AnnotationConfigApplicationContext("com.twuc.webApp.simplest");
        PrototypeScopeDependsOnSingleton pc1 = ctx.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton pc2 = ctx.getBean(PrototypeScopeDependsOnSingleton.class);
        Logger2 logger = ctx.getBean(Logger2.class);
        assertEquals(
                "create normal class once\n" +
                "create singleton once\n" +
                "create singleton once\n",logger.getLog());
    }




}
