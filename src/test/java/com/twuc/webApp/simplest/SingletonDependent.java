package com.twuc.webApp.simplest;

import com.twuc.webApp.simplest.scanning.Logger;
import com.twuc.webApp.simplest.scanning.Logger2;
import org.springframework.stereotype.Component;

@Component
public class SingletonDependent {

    public SingletonDependent(Logger2 logger) {
        logger.log("create normal class once");
    }
}
