package com.twuc.webApp.simplest;

import com.twuc.webApp.simplest.scanning.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InterfaceOneImpl implements InterfaceOne{

    public InterfaceOneImpl(Logger logger){
        logger.log("created normal class");
    }
}
