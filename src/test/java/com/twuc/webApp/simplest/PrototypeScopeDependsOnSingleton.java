package com.twuc.webApp.simplest;

import com.twuc.webApp.simplest.scanning.Logger;
import com.twuc.webApp.simplest.scanning.Logger2;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class PrototypeScopeDependsOnSingleton {
    private SingletonDependent singletonDependent;
    public PrototypeScopeDependsOnSingleton(SingletonDependent singletonDependent, Logger2 logger){
        this.singletonDependent = singletonDependent;
        logger.log("create singleton once");
    }
}
