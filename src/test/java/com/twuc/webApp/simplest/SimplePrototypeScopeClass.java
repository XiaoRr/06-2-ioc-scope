package com.twuc.webApp.simplest;

import com.twuc.webApp.simplest.scanning.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import sun.rmi.runtime.Log;

@Component
@Scope("prototype")
public class SimplePrototypeScopeClass {

    public SimplePrototypeScopeClass(Logger log) {
        log.log("created prototype class");
    }
}
