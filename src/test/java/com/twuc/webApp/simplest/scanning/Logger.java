package com.twuc.webApp.simplest.scanning;

import org.springframework.stereotype.Component;

@Component
public class Logger {

    public String getLog() {
        return log;
    }

    private static String log;

    Logger(){
        log = "";
    }

    public void log(String log){
        this.log = this.log+(log+'\n');
    }

}
