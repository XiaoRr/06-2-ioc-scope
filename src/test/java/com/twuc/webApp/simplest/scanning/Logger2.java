package com.twuc.webApp.simplest.scanning;

import org.springframework.stereotype.Component;

@Component
public class Logger2 {

    public String getLog() {
        return log;
    }

    private static String log;

    Logger2(){
        log = "";
    }

    public void log(String log){
        this.log = this.log+(log+'\n');
    }

}
